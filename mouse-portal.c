/*
 * Copyright 2020 Yunxiang Li
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 199309L

#include <X11/Xlib.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_SCREEN 16
#define DEFAULT_RATE 1
#define NSEC_IN_SEC 1000000000

volatile sig_atomic_t done = 0;

typedef struct {
  int w, h;
} screen_t;

void sigterm(int signum) { done = 1; }

void usage(char *argv[]) {
  fprintf(stderr,
          "Usage: %s [-r rate] <res_1> <res_2> ...\n"
          " rate: refresh per second\n"
          "  res: X Y dimensions separated with 'x'\n",
          argv[0]);
}

int main(int argc, char *argv[]) {

  if (argc == 1) {
    usage(argv);
    return EXIT_SUCCESS;
  } else if (argc < 3) {
    fprintf(stderr, "Not enough screens\n");
    return EXIT_FAILURE;
  } else if (argc > MAX_SCREEN + 1) {
    fprintf(stderr, "Too many screens\n");
    return EXIT_FAILURE;
  }

  char **list = argv + 1;
  int width = 0, screen_count = argc - 1;
  long sleep_time = NSEC_IN_SEC / DEFAULT_RATE;
  screen_t screens[MAX_SCREEN];
  if (!strcmp("-r", list[0])) {
    sleep_time = atol(list[1]);
    if (sleep_time == 0) {
      usage(argv);
      return EXIT_FAILURE;
    }
    sleep_time = NSEC_IN_SEC / sleep_time;
    list += 2;
    screen_count -= 2;
  }
  for (int i = 0; i < screen_count; ++i) {
    char *w = list[i];
    char *h = strchr(w, 'x');
    if (h == NULL || *(h + 1) == '\0') {
      usage(argv);
      return EXIT_FAILURE;
    }
    *h++ = '\0';

    screens[i].w = atoi(w);
    screens[i].h = atoi(h);
    if (screens[i].w == 0 || screens[i].h == 0) {
      usage(argv);
      return EXIT_FAILURE;
    }
    width += screens[i].w;
    screens[i].w = width;
  }

  Display *display = XOpenDisplay(NULL);
  if (!display) {
    fprintf(stderr, "Failed to get display\n");
    return EXIT_FAILURE;
  }
  signal(SIGINT, sigterm);
  signal(SIGTERM, sigterm);
  Window window = XRootWindow(display, 0);
  int cur_screen = 0;
  size_t garbage;
  void *null = &garbage;
  while (!done) {
    int x, y, new_x, new_y;
    XQueryPointer(display, window, null, null, &x, &y, null, null, null);
    for (int i = 0; i < screen_count; ++i) {
      if (x <= screens[i].w) {
        if (x == screens[i].w - 1 && i != screen_count - 1) {
          new_x = x + 2;
          ++i;
        } else if (x == screens[i].w) {
          new_x = x - 2;
        } else {
          new_x = x;
        }
        if (cur_screen != i) {
          new_y = y * screens[i].h / screens[cur_screen].h;
          XWarpPointer(display, None, window, x, y, 0, 0, new_x, new_y);
          cur_screen = i;
        }
        break;
      }
    }

    struct timespec rem, req = {0, sleep_time};
    nanosleep(&req, &rem);
  }

  XCloseDisplay(display);
  return EXIT_SUCCESS;
}
