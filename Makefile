#
# Copyright 2020 Yunxiang Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

CXX = gcc
CXXFLAGS = -Os -pedantic -Wall -fno-stack-protector -ffunction-sections -fdata-sections -fno-math-errno -fmerge-all-constants -fno-ident -Wl,--gc-sections,-z,norelro,--build-id=none
LDFLAGS = -lX11

mouse-portal : mouse-portal.c
	${CXX} ${CXXFLAGS} mouse-portal.c -o mouse-portal ${LDFLAGS}
	strip -S --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id --remove-section=.note.ABI-tag mouse-portal

.PHONY : debug

debug : mouse-portal.c
	${CXX} -g ${CXXFLAGS} mouse-portal.c -o mouse-portal
